package com.devcamp.s50.task5820.restapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task5820.restapi.model.CDrink;
import com.devcamp.s50.task5820.restapi.respository.IDrinkRepository;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CDrinkController {
    @Autowired
    IDrinkRepository iDrinkRepository;

    @CrossOrigin
    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllCDrinks(){
        try {
            List<CDrink> listDrink = new ArrayList<CDrink>();
            iDrinkRepository.findAll().forEach(listDrink::add);
            return new ResponseEntity<List<CDrink>>(listDrink, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
